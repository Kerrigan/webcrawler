import json
import requests
import time
from bs4 import BeautifulSoup, Comment

# Initialise a dict for the Inverted index
wordlist = dict()


# For each word in the find request search for all pages the word appears on
# return a list of the words mapped to pages
def find(userrequest):
    try:
        pages = dict()

        for word in userrequest:
            occurrences = wordlist.get(word)
            page_occurrence = []
            for occurrence in occurrences:
                page_occurrence.append(occurrence)
            pages[word] = page_occurrence

        page_list = []
        first = True

        for word in pages:
            if first:
                for page in pages[word]:
                    page_list.append(page)
                first = False
            else:
                pages_remove = []
                for page in page_list:
                    if page not in pages[word]:
                        pages_remove.append(page)
                for page in pages_remove:
                    page_list.remove(page)
        # Return list of pages
        return page_list

    except:
        # Otherwise return a blank array
        return []


# Print all of the page occurrences for a requested word
def printword(word):
    # Make sure an inverted index is loaded/built
    if len(wordlist) > 0:
        try:
            print("Occurrences of the word", word, "at example.webscraping.com")
            occurrences = wordlist.get(word)
            return occurrences
        except:
            print("Word not found")
    else:
        print("No wordlist detected, load or build the inverted index")


# Checks whether an element is visible on a page or not, skips words that aren't visible to the user
def visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True


# Saves the inverted index to a json file
def save(obj):
    with open('invindex.json', 'w') as fp:
        json.dump(obj, fp)


# Loads an inverted index from a json file
def load():
    try:
        with open('invindex.json', 'r') as fp:
            return json.load(fp)
    except:
        return "Error loading file"


def build():
    wordlist.clear()
    print("\nStart of Build\n")

    pagelist = set()
    newpagelist = set("/")
    pages = 0

    # While there are still new pages to visit
    while len(newpagelist) > 0:

        new_pages = set()

        for page in newpagelist:

            request = requests.get("http://example.webscraping.com" + page)

            if not request.status_code == 200:
                print("Issue retrieving page", page)
                continue

            # Make sure the page hasn't already been visited by checking against pagelist
            if request.url.replace("http://example.webscraping.com", "") in pagelist:
                # print("Page", request.url, "already visited for", page)
                continue
            else:
                pagelist.add(page)

            new_soup = BeautifulSoup(request.text, 'html.parser')

            pages += 1
            print("[", pages, "]")

            # Get a list of words found on the webpage
            # User a filter to remove text found from the webpage that is not visible to the user
            visabletext = filter(visible, new_soup.findAll(text=True))
            page_words = []

            # Add each word in the visible text to a list
            for text in visabletext:
                words = text.strip().split()
                for word in words:
                    page_words.append(word)

            # For each of the new words add the value to the inverted index for page occurrence of the words
            for word in page_words:
                if word in wordlist:
                    if page in wordlist[word]:
                        wordlist[word][page] += 1
                    else:
                        wordlist[word].update({page: 1})
                else:
                    # print("New word", word, "added")
                    wordlist[word] = {page: 1}

            # For every link on the page check if it has new pages to visit, add them to the new page set
            for link in set(new_soup.find_all("a")):

                linkurl = link['href']

                if linkurl == "index":
                    linkurl = linkurl.split("index")[0]

                # The '?' symbol is used to store the original page when clicking on the login link
                # When the user logs in it redirects them back to the page they were on before
                if "?" in linkurl:
                    linkurl = linkurl.split("?")[0]

                # Skips iso links
                if "iso" in linkurl:
                    continue

                # Skip edit links - Redirects to login page
                if linkurl.__contains__("edit"):
                    continue

                new_pages.add(linkurl)

            print("Visited " + page + "\n")
            time.sleep(1)

        newpagelist.clear()

        # Add the new pages to the whole page list to be crawled
        for page in new_pages:
            if page not in pagelist:
                newpagelist.add(page)

    # print(wordlist.items())

    # Save the index to a file
    save(wordlist)

    print("\nEnd of Build\n")


# Take user input and execute commands
while True:
    userinput = input("\nEnter Command:")

    # Split user input
    strings = userinput.split()

    if len(strings):

        # Check which command the user has entered
        if strings[0] == 'exit':
            break

        elif strings[0] == 'build':
            build()

        elif strings[0] == 'load':
            wordlist = load()

            if wordlist == "Error loading file":
                print(wordlist)
                wordlist = {}
            else:
                print("Word list loaded from memory")

        elif strings[0] == 'print':
            occurrences = printword(strings[1])
            n = 0
            for occurrence in occurrences:
                n += 1
                print(n, occurrence)

        elif strings[0] == 'find':
            results = find(strings[1:])
            n = 0
            for result in results:
                n += 1
                print(n, result)
        else:
            print('Invalid Command! Type help for a list of commands\n')
    else:
        print("No command entered!\n")
